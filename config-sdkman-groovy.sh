#!/bin/bash

curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
#CHANGE SDKMAN configureation to enable quiet mode installation
sed -i -e 's/sdkman_auto_answer=false/sdkman_auto_answer=true/g' /$HOME/.sdkman/etc/config
sdk install groovy
groovy -version
