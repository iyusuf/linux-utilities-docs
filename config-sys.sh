#!/bin/bash



#stop iptables firewall
service iptables stop
#make sure it doesn't start at next boot
chkconfig iptables off
# yum update && yum upgrade
yum update && yum upgrade
#install unzip which we will need
yum install unzip -y
# install git
yum install git -y
# install Subversion
yum install svn -y
# install wget
yum install wget -y

#install vim
yum install vim -y
