#!/usr/bin/env bash

cd ~
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u111-b14/jdk-8u111-linux-x64.rpm"

sudo yum localinstall jdk-8u111-linux-x64.rpm -y 
rm jdk-8u111-linux-x64.rpm
